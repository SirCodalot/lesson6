#include <iostream>

#define INVALID_NUMBER_EXCEPTION std::string("This user is not authorized to access 8200, please enter different numbers, or try to get clearance in 1 year.")

int add(int a, int b) {
	if (a == 8200 || b == 8200 || a + b == 8200)
		throw INVALID_NUMBER_EXCEPTION;
	return a + b;
}

int  multiply(int a, int b) {
	int sum = 0;
	for (int i = 0; i < b; i++) {
		sum = add(sum, a);
	};
	if (a == 8200 || b == 8200 || sum == 8200)
		throw INVALID_NUMBER_EXCEPTION;
	return sum;
}

int  pow(int a, int b) {
	int exponent = 1;
	for (int i = 0; i < b; i++) {
		exponent = multiply(exponent, a);
	};
	if (a == 8200 || b == 8200 || exponent == 8200)
		throw INVALID_NUMBER_EXCEPTION;
	return exponent;
}

int main(void) {
	try
	{
		std::cout << add(5, 5) << std::endl;
		std::cout << add(8200, 5) << std::endl;
	}
	catch (const std::string s)
	{
		std::cout << s.c_str() << std::endl << std::endl;
	}

	try
	{
		std::cout << multiply(5, 5) << std::endl;
		std::cout << multiply(8200, 5) << std::endl;
	}
	catch (const std::string s)
	{
		std::cout << s.c_str() << std::endl << std::endl;
	}

	try
	{
		std::cout << pow(5, 5) << std::endl;
		std::cout << pow(8200, 5) << std::endl;
	}
	catch (const std::string s)
	{
		std::cout << s.c_str() << std::endl;
	}

	system("pause");
	return 0;
}