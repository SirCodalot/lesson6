#include <iostream>

#define ERROR_8200 "This user is not authorized to access 8200, please enter different numbers, or try to get clearance in 1 year."
#define VALID new bool(true)

int add(int a, int b, bool* valid) {
	if (a == 8200 || b == 8200 || a + b == 8200)
	{
		if (*valid)
			std::cout << ERROR_8200 << std::endl;
		*valid = false;
		a = 0;
		b = 0;
	}
	return a + b;
}

int multiply(int a, int b, bool* valid) {
	int sum = 0;
	for (int i = 0; i < b; i++) {
		sum = add(sum, a, valid);
	};
	if (a == 8200 || b == 8200 || sum == 8200)
	{
		if (*valid)
			std::cout << ERROR_8200 << std::endl;
		*valid = false;
		sum = 0;
	}
	return sum;
}

int pow(int a, int b, bool* valid) {
	int exponent = 1;
	for (int i = 0; i < b; i++) {
		exponent = multiply(exponent, a, valid);
	};
	if (a == 8200 || b == 8200 || exponent == 8200)
	{
		if (*valid)
			std::cout << ERROR_8200 << std::endl;
		*valid = false;
		exponent = 0;
	}
	return exponent;
}

int main(void) {
	std::cout << add(5, 5, VALID) << std::endl;
	std::cout << add(8200, 5, VALID) << std::endl << std::endl;

	std::cout << multiply(5, 5, VALID) << std::endl;
	std::cout << multiply(8200, 5, VALID) << std::endl << std::endl;

	std::cout << pow(5, 5, VALID) << std::endl;
	std::cout << pow(8200, 5, VALID) << std::endl;

	system("pause");
	return 0;
}