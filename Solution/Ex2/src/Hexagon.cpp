#include "Hexagon.h"
#include "shapeexception.h"

Hexagon::Hexagon(std::string one, std::string two, double side) : Shape(one, two)
{
	if (side < 0)
		throw shapeException();
	_side = side;
}

double Hexagon::CalArea()
{
	return MathUtils().calHexagonArea(_side);
}

void Hexagon::draw()
{
	std::cout << getName() << std::endl << getColor() << std::endl << "Side is " << _side << std::endl << "area is " << CalArea() << std::endl;
}


double Hexagon::getSide() const
{
	return _side;
}

void Hexagon::setSide(double side)
{
	if (side < 0)
		throw shapeException();
	_side = side;
}
