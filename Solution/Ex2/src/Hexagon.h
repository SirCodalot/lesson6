#ifndef HEXAGON_H
#define HEXAGON_H

#include "MathUtils.h"
#include "shape.h"

class Hexagon : public Shape
{
public:
	Hexagon(std::string, std::string, double side);

	void draw() override;
	double CalArea() override;

	void setSide(double side);
	double getSide() const;

private:
	double _side;
};

#endif