#include "MathUtils.h"

double MathUtils::calHexagonArea(double side)
{
	return 3 * sqrt(3) / 2 * pow(side, 2);
}

double MathUtils::calPentagonArea(double side)
{
	return 0.25 * sqrt(5 * (5 + 2 * sqrt(5))) * pow(side, 2);
}
