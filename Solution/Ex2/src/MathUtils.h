#ifndef MATHUTILS_H
#define MATHUTILS_H

#include <math.h>

class MathUtils
{
public:
	static double calPentagonArea(double side);
	static double calHexagonArea(double side);
};

#endif