#include "Pentagon.h"
#include "shapeexception.h"

Pentagon::Pentagon(std::string one, std::string two, double side) : Shape(one, two)
{
	if (side < 0)
		throw shapeException();
	_side = side;
}


double Pentagon::CalArea()
{
	return MathUtils().calPentagonArea(_side);
}

void Pentagon::draw()
{
	std::cout << getName() << std::endl << getColor() << std::endl << "Side is " << _side << std::endl << "area is " << CalArea() << std::endl;
}


double Pentagon::getSide() const
{
	return _side;
}

void Pentagon::setSide(double side)
{
	if (side < 0)
		throw shapeException();
	_side = side;
}
