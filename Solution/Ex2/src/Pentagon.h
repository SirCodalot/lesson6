#ifndef PENTAGON_H
#define PENTAGON_H

#include "MathUtils.h"
#include "shape.h"

class Pentagon : public Shape
{
public:
	Pentagon(std::string, std::string, double side);

	void draw() override;
	double CalArea() override;

	void setSide(double side);
	double getSide() const;

private:
	double _side;
};

#endif
