#include <exception>

class inputException : public std::exception
{
public:
	virtual char const* what() const
	{
		return "This is an input exception!";
	}
};