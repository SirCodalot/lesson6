#include <iostream>
#include "shape.h"
#include "circle.h"
#include "quadrilateral.h"
#include "rectangle.h"
#include "parallelogram.h"
#include <string>
#include "shapeexception.h"
#include "inputexception.h"
#include "Hexagon.h"
#include "Pentagon.h"

bool isString();
void checkCin();

int main()
{
	std::string nam, col; double rad = 0, ang = 0, ang2 = 180; int height = 0, width = 0;
	Circle circ(col, nam, rad);
	quadrilateral quad(nam, col, width, height);
	rectangle rec(nam, col, width, height);
	parallelogram para(nam, col, width, height, ang, ang2);
	Pentagon pent(nam, col, width);
	Hexagon hex(nam, col, width);

	Shape *ptrcirc = &circ;
	Shape *ptrquad = &quad;
	Shape *ptrrec = &rec;
	Shape *ptrpara = &para;
	Shape *ptrpent = &pent;
	Shape *ptrhex = &hex;

	std::cout << "Enter information for your objects" << std::endl;
	const char circle = 'c', quadrilateral = 'q', rectangle = 'r', parallelogram = 'p', pentagon = 'f', hexagon = 'h'; char shapetype;
	char x = 'y';
	while (x != 'x') {
		std::cout << "which shape would you like to work with?.. \nc=circle, q = quadrilateral, r = rectangle, p = parallelogram, f = pentagon, h = hexagon" << std::endl;
		try
		{
			std::cin >> shapetype;
			isString();
		} catch(inputException e)
		{
			printf(e.what());
		}
		try
		{

			switch (shapetype) {
			case pentagon:
				std::cout << "enter color, name, side for pentagon" << std::endl;
				try
				{
					std::cin >> col >> nam >> width;
					checkCin();
				} 
				catch(inputException e)
				{
					printf(e.what());
				}
				try
				{
					pent.setSide(width);
				}
				catch(shapeException e)
				{
					printf(e.what());
				}
				ptrpent->draw();
				break;
			case hexagon:
				std::cout << "enter color, name, side for hexagon" << std::endl;
				try
				{
					std::cin >> col >> nam >> width;
					checkCin();
				}
				catch (inputException e)
				{
					printf(e.what());
				}
				try
				{
					hex.setSide(width);
				}
				catch (shapeException e)
				{
					printf(e.what());
				}
				ptrhex->draw();
				break;
			case 'c':
				std::cout << "enter color, name,  rad for circle" << std::endl;
				try
				{
					std::cin >> col >> nam >> rad;
					checkCin();
				}
				catch (inputException e)
				{
					printf(e.what());
				}
				circ.setColor(col);
				circ.setName(nam);
				try
				{
					circ.setRad(rad);
				}
				catch (shapeException e)
				{
					printf(e.what());
				}
				ptrcirc->draw();
				break;
			case 'q':
				std::cout << "enter name, color, height, width" << std::endl;
				try
				{
					std::cin >> nam >> col >> height >> width;
					checkCin();
				}
				catch (inputException e)
				{
					printf(e.what());
				}
				try
				{
					quad.setName(nam);
					quad.setColor(col);
					quad.setHeight(height);
					quad.setWidth(width);
				}
				catch(shapeException e)
				{
					printf(e.what());
				}
				ptrquad->draw();
				break;
			case 'r':
				std::cout << "enter name, color, height, width" << std::endl;
				try
				{
					std::cin >> nam >> col >> height >> width;
					checkCin();
				}
				catch (inputException e)
				{
					printf(e.what());
				}
				try
				{
					rec.setName(nam);
					rec.setColor(col);
					rec.setHeight(height);
					rec.setWidth(width);
				}
				catch(shapeException e)
				{
					printf(e.what());
				}
				ptrrec->draw();
				break;
			case 'p':
				std::cout << "enter name, color, height, width, 2 angles" << std::endl;
				try
				{
					std::cin >> nam >> col >> height >> width >> ang >> ang2;
					checkCin();
				}
				catch (inputException e)
				{
					printf(e.what());
				}
				try
				{
					para.setName(nam);
					para.setColor(col);
					para.setHeight(height);
					para.setWidth(width);
					para.setAngle(ang, ang2);
				} 
				catch(shapeException e)
				{
					printf(e.what());
				}
				ptrpara->draw();

			default:
				std::cout << "you have entered an invalid letter, please re-enter" << std::endl;
				break;
			}
			std::cout << "would you like to add more object press any key if not press x" << std::endl;
			try
			{
				std::cin >> x;
				isString();
			}
			catch (inputException e)
			{
				printf(e.what());
			}
		}
		catch (std::exception e)
		{
			printf(e.what());
		}
		catch (...)
		{
			printf("caught a bad exception. continuing as usual\n");
		}
	}

	system("pause");
	return 0;

}

bool isString()
{
	if (std::cin.peek() != '\n')
	{
		std::cin.clear();
		std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
		throw inputException();
	}
}


void checkCin()
{
	if (std::cin.fail())
	{
		std::cin.clear();
		std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
		throw inputException();
	}
}
