#include <exception>

class shapeException : public std::exception
{
public:
	virtual char const* what() const
	{
		return "This is a shape exception!";
	}
};